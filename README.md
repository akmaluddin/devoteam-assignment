This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

---

Endpoints are statically build using nextjs built in api functions

There are two general microservices which are responsible for their own DB collection (Using static JSON)

Questions are indexable by a unique identifier, likewise each answers have their own associated UID

two main APIs, questions and validate accessible by calling localhost:3000/api/questions / localhost:3000/api/validate

Some major considerations to be made if this is to move to prod

There is a need to add validators to validate params. Errors are not exhaustively managed. Network delay is simulated by a simple timeout

---

For frontend side-effects are kinda implemented using two different implementations, one is react-query-esque while the other sort of follows redux-saga redux-thunk styles whereby each async function will update a shared state

state is built using context api and a simple reducer with associated dispatcher function

---

Some pitfalls integrating tests, since the assignment had an emphasis on reducing third party code, all my REST apis are called using node-fetch.

Setting up the tests and mocks doesn't seem to go my way as [discussed here](https://github.com/vercel/next.js/discussions/13678#discussioncomment-22383).

I would probably set it up using axios and MSW to mock the api responses, sorry 🙏
