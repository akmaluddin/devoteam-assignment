type FetchStatus = "loading" | "success" | "error";

type FetchResponse<T> = {
    status?: FetchStatus;
    data?: T;
    errMsg?: string;
};

type QuestionStatuses =
    | "incorrect"
    | "correct"
    | "skipped"
    | "loading"
    | "error";
