export type Question = {
    question: string;
    imageSrc?: string;
    altText?: string;
    possibleAnswers: PossibleAnswers[];
};

export type PossibleAnswers = {
    id: string;
    value: string;
};

export type Questions = Record<string, Question>;
