import { useEffect, useState } from "react";

export const useQuery = <T extends unknown>(
    url: string,
    isEnabled: boolean = true
): FetchResponse<T> => {
    const [status, setStatus] = useState<FetchStatus | undefined>(undefined);
    const [data, setData] = useState<T | undefined>(undefined);
    const [errMsg, setErrMsg] = useState<string | undefined>(undefined);

    useEffect(() => {
        const controller = new AbortController();

        if (isEnabled) {
            setStatus("loading");
            fetch(url, {
                signal: controller.signal,
            })
                .then((res) => {
                    if (res.ok) {
                        return res.json();
                    }

                    return res.json().then((err) => Promise.reject(err));
                })
                .then((data) => {
                    setStatus("success");
                    setData(data);
                })
                .catch((err) => {
                    setErrMsg(err.message || err);
                    setStatus("error");
                });
        }

        return () => {
            controller.abort();
        };
    }, [isEnabled]);

    return {
        status,
        data,
        errMsg,
    };
};
