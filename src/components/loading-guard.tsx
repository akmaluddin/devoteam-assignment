import type { ReactNode, FC } from "react";

interface LoadingGuardProps {
    children?: ReactNode;
    status?: FetchStatus;
}

export const LoadingGuard: FC<LoadingGuardProps> = ({ children, status }) => (
    <div className="flex flex-col gap-4 p-4">
        {status === "success" ? (
            children
        ) : status === "error" ? (
            <div>Ooops! some error</div>
        ) : (
            <div>Loading</div>
        )}
    </div>
);
