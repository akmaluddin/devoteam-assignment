import {
    useReducer,
    createContext,
    useContext,
    useEffect,
    type ReactNode,
    type ReactNodeArray,
} from "react";
import { validateAnswer } from "services/validate";

type PageState = {
    questionResults?: Record<string, Omit<QuestionResult, "questionId">>;
    fiftyFiftyAvailable: boolean;
    plus10Available: boolean;
};

const InitialState: PageState = {
    fiftyFiftyAvailable: true,
    plus10Available: true,
};

export type QuestionResult = {
    questionId: string;
    status: QuestionStatuses;
    lockedInAnswer?: "string";
    timeLeft?: number;
};

export enum PageActions {
    SET_VALIDATED_RESULT,
    SET_PROGRESS,
    USE_50_50,
    USE_PLUS_10,
    RESTORE_50_50,
}

type ActionDispatch =
    | {
          type: PageActions.SET_VALIDATED_RESULT;
          payload: any;
      }
    | {
          type: PageActions.SET_PROGRESS;
          payload: any;
      }
    | {
          type: PageActions.USE_PLUS_10;
      }
    | {
          type: PageActions.USE_50_50;
      }
    | {
          type: PageActions.RESTORE_50_50;
      };

export type Dispatcher = (input: ActionDispatch) => void;

const reducer = (state: PageState, action: ActionDispatch) => {
    switch (action.type) {
        case PageActions.SET_PROGRESS:
            return {
                ...state,
                questionResults: {
                    ...state.questionResults,
                    [action.payload.questionId]: {
                        ...action.payload,
                    },
                },
            };

        case PageActions.SET_VALIDATED_RESULT:
            return {
                ...state,
                questionResults: {
                    ...state.questionResults,
                },
            };

        case PageActions.USE_50_50:
            return {
                ...state,
                fiftyFiftyAvailable: false,
            };

        case PageActions.RESTORE_50_50:
            return {
                ...state,
                fiftyFiftyAvailable: true,
            };

        case PageActions.USE_PLUS_10:
            return {
                ...state,
                plus10Available: false,
            };

        default:
            return state;
    }
};

export const PageContext = createContext<{
    pageState: PageState;
    dispatch: Dispatcher;
}>({ pageState: InitialState, dispatch: () => {} });

export const PageProvider = ({
    children,
}: {
    children: ReactNode | ReactNodeArray;
}) => {
    const [pageState, dispatch] = useReducer(reducer, InitialState);

    return (
        <PageContext.Provider value={{ pageState, dispatch }}>
            {children}
        </PageContext.Provider>
    );
};

export const usePageProvider = () => useContext(PageContext);
