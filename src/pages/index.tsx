import type { NextPage } from "next";
import Head from "next/head";
import Image from "next/image";
import { Question } from "containers/question";
import { useGetQuestions } from "services/questions";
import {
  useEffect,
  useState,
  createRef,
  useRef,
  type ReactNodeArray,
} from "react";
import { PageProvider } from "contexts";
import { LoadingGuard } from "components/loading-guard";
import { Summary } from "containers/summary";

const Home: NextPage = () => {
  const { data, status } = useGetQuestions();
  const [questions, setQuestions] = useState<ReactNodeArray>([]);
  const [start, setStart] = useState<boolean>(false);
  const ref = useRef<HTMLDivElement>(null);

  const handleProceed = () => {
    setQuestions((prevState) => {
      ref.current?.scrollIntoView();
      const index = prevState.length;

      return index === data?.questions.length
        ? prevState
        : [
            ...prevState,
            <Question
              key={data!.questions[index]}
              questionId={data!.questions[index]}
              handleProceed={handleProceed}
            />,
          ];
    });
  };

  return (
    <PageProvider>
      <Head>
        <title>QUICQUIZ</title>
        <meta name="description" content="Quiz take home assignment" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className="flex flex-col gap-4 p-4">
        <LoadingGuard status={status}>
          <div className="bg-white p-[2rem] md:p-[8rem] rounded-md drop-shadow-xl flex flex-col items-center gap-4">
            <div className="text-4xl font-bold">Welcome to the QUICQUIZ</div>
            <div className="text-center text-sm">
              You will be presented with questions in randomly generated order.
              <br />
              You are to choose from one of the 4 possible answers provided
              <br />
              The answer positions are randomly generated
              <br />
              You will be given a total of 15 seconds to answer each question,
              <br />
              if you can't it will automatically skip to the next one
              <br />
              You are provided two lifelines per game :-
              <br />
              <br />
              Add 10s to your current timer
              <br />
              Remove half of the choices, reducing to a probability of 50%
            </div>
            <button
              onClick={() => {
                setStart(true);
                handleProceed();
              }}
              disabled={start}
              className="bg-gradient-to-br from-rose-500 to-red-900 disabled:from-slate-500 disabled:to-slate-900 py-[1rem] px-[4rem] text-white font-bold border-4 border-gray-300 border-solid rounded-md"
            >
              Start
            </button>
          </div>
        </LoadingGuard>
        {questions}
        <Summary questionLengths={data?.questions.length} />
        <div ref={ref} className="pt-[28rem]" />
      </div>
    </PageProvider>
  );
};

export default Home;
