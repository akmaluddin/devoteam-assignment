import type { NextApiRequest, NextApiResponse } from "next";
import answers from "./answers.json";

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    const { questionId, lockedInAnswer } = JSON.parse(req.body);

    const { answerId } = (answers as Record<string, any>)[questionId];

    if (req.method === "POST") {
        res.status(200).json({
            result: answerId === lockedInAnswer ? "correct" : "incorrect",
        });
    } else {
        res.status(404);
    }
};

export default handler;
