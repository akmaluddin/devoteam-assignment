import type { NextApiRequest, NextApiResponse } from "next";
import answers from "./answers.json";
import questions from "../questions/questions.json";
import type { Questions } from "../../../types/questions";

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    const questionsData = questions as unknown as Questions;
    const { questionId } = JSON.parse(req.body);

    const { answerId } = (answers as Record<string, any>)[questionId];

    const possibleAnswers = questionsData[questionId].possibleAnswers
        .map((answer) => answer.id)
        .filter((id) => id !== answerId);

    const randomizedAnswer =
        possibleAnswers[Math.floor(Math.random() * possibleAnswers.length)];

    res.status(200).json({
        result:
            Math.random() < 0.5
                ? [randomizedAnswer, answerId]
                : [answerId, randomizedAnswer],
    });
};

export default handler;
