import { createMocks } from "node-mocks-http";
import handleValidate from "./index";
import handle50 from "./fifty";
// import handleQuestionById from "./[id]";

const element = {
    questionId: "0001",
    answerId: "0003",
    incorrectId: "0001",
    property: "result",
    correct: "correct",
    incorrect: "incorrect",
} as const;

describe("/api/validate", () => {
    test("validates the answer correctly", async () => {
        const { req, res } = createMocks({
            method: "POST",
            body: JSON.stringify({
                questionId: element.questionId,
                lockedInAnswer: element.answerId,
            }) as unknown as Body,
        });

        await handleValidate(req, res);

        const response = JSON.parse(res._getData());

        expect(response).toHaveProperty(element.property);

        expect(response[element.property]).toEqual(element.correct);
    });

    test("validates the answer incorrectly", async () => {
        const { req, res } = createMocks({
            method: "POST",
            body: JSON.stringify({
                questionId: element.questionId,
                lockedInAnswer: element.incorrectId,
            }) as unknown as Body,
        });

        await handleValidate(req, res);

        const response = JSON.parse(res._getData());

        expect(response).toHaveProperty(element.property);

        expect(response[element.property]).toEqual(element.incorrect);
    });
});

describe("/api/validate/fifty", () => {
    test("it returns 2 choices whereby one of them is the correct answer", async () => {
        const { req, res } = createMocks({
            method: "POST",
            body: JSON.stringify({
                questionId: element.questionId,
            }) as unknown as Body,
        });

        await handle50(req, res);

        const response = JSON.parse(res._getData());

        expect(response).toHaveProperty(element.property);

        expect(response[element.property]).toHaveLength(2);
        expect(new Set(response[element.property]).size).toEqual(2);

        expect(response[element.property]).toContain(element.answerId);
    });
});
