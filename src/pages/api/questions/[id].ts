import type { NextApiRequest, NextApiResponse } from "next";
import type { Question, Questions } from "../../../types/questions";
import questions from "./questions.json";

const durstenfeldShuffle = (questionsData: any[]) => {
    for (let i = questionsData.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [questionsData[i], questionsData[j]] = [
            questionsData[j],
            questionsData[i],
        ];
    }

    return questionsData;
};

const handler = (req: NextApiRequest, res: NextApiResponse) => {
    const questionId = req.query.id as string;
    const questionsData = questions as unknown as Questions;
    if (req.method === "GET") {
        try {
            if (!questionsData[questionId])
                throw new Error("question does not exist");

            res.status(200).json({
                questionId,
                question: {
                    ...questionsData[questionId],
                    possibleAnswers: durstenfeldShuffle(
                        questionsData[questionId].possibleAnswers
                    ),
                },
            });
        } catch (e) {
            let message = "Unknown Error";
            if (e instanceof Error) message = e.message;
            res.status(404).json({ errorMessage: message });
        }
    } else {
        res.status(404);
    }
};

export default handler;
