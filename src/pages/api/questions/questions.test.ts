import { createMocks } from "node-mocks-http";
import handleQuestions from "./index";
import handleQuestionById from "./[id]";

describe("/api/questions", () => {
    const element = {
        length: 10,
        property: "questions",
    } as const;

    test("returns array of unique question ids", async () => {
        const { req, res } = createMocks({
            method: "GET",
        });

        await handleQuestions(req, res);

        const questionIdList = JSON.parse(res._getData());

        expect(questionIdList).toHaveProperty(element.property);

        expect(questionIdList.questions).toHaveLength(element.length);

        expect(new Set(questionIdList.questions).size).toEqual(element.length);
    });
});

describe("/api/questions/[id]", () => {
    const element = {
        id: "0002",
        possibleAnswerProp: "possibleAnswers",
        questionProp: "question",
        answerLength: 4,
    } as const;

    test("return question data by id", async () => {
        const { req, res } = createMocks({
            method: "GET",
            query: {
                id: element.id,
            },
        });

        await handleQuestionById(req, res);

        const questionById = JSON.parse(res._getData());

        expect(questionById.question).toHaveProperty(
            element.possibleAnswerProp
        );

        expect(questionById.question[element.possibleAnswerProp]).toHaveLength(
            element.answerLength
        );
    });
});
