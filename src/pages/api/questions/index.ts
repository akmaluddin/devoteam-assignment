import type { NextApiRequest, NextApiResponse } from "next";
import type { Questions } from "../../../types/questions";
import questions from "./questions.json";

const DEFAULT_QUESTION_LENGTH = 10;

const durstenfeldShuffle = (questionsData: string[]) => {
    for (let i = questionsData.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [questionsData[i], questionsData[j]] = [
            questionsData[j],
            questionsData[i],
        ];
    }

    return questionsData;
};

const handler = async (
    req: NextApiRequest,
    res: NextApiResponse<{ questions: string[] }>
) => {
    const questionsData = questions as unknown as Questions;

    if (req.method === "GET") {
        const questionsShuffled = durstenfeldShuffle(
            Object.keys(questionsData)
        ).slice(0, DEFAULT_QUESTION_LENGTH);
        res.status(200).json({ questions: questionsShuffled });
    } else {
        res.status(404);
    }
};

export default handler;
