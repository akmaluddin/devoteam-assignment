import { render, act, screen } from "@testing-library/react";
import { Question } from "./question";
import userEvent from "@testing-library/user-event";

const responseMock = jest.fn();

global.fetch = jest.fn(() =>
    Promise.resolve({
        ok: true,
        json: () => responseMock,
    })
) as jest.Mock;

jest.useFakeTimers();

describe("<Question />", () => {
    const element = {
        add10sButton: "add-10-s",
        fifty50Button: "perform-50-50",
    } as const;

    const BaseMock = {
        questionId: "0011",
        question: {
            question: "question-name",
            possibleAnswers: [
                {
                    id: "0002",
                    value: "some-answer-1",
                },
                {
                    id: "0004",
                    value: "some-answer-2",
                },
                {
                    id: "0001",
                    value: "some-answer-3",
                },
                {
                    id: "0003",
                    value: "some-answer-4",
                },
            ],
        },
    };

    it("renders accordingly", async () => {
        responseMock
            .mockReturnValueOnce(BaseMock)
            .mockReturnValueOnce({ result: "correct" });
        const mockCallback = jest.fn();

        await act(() => {
            render(
                <Question
                    questionId={"some-val"}
                    handleProceed={mockCallback}
                />
            );
        });

        expect(screen.getByText(BaseMock.question.question)).toBeDefined();
        expect(screen.queryByText(BaseMock.questionId)).toBeNull();
        expect(screen.getByTestId(element.add10sButton)).toBeDefined();
        expect(screen.getByTestId(element.fifty50Button)).toBeDefined();
    });
});
