import { type FC } from "react";
import { usePageProvider } from "contexts";

interface SummaryProps {
    questionLengths?: any;
}

export const Summary: FC<SummaryProps> = ({ questionLengths }) => {
    const {
        pageState: { questionResults = {}, plus10Available },
    } = usePageProvider();

    const incorrect = Object.values(questionResults).filter(
        (ans) => ans.status === "incorrect"
    );

    const correct = Object.values(questionResults).filter(
        (ans) => ans.status === "correct"
    );

    const skipped = Object.values(questionResults).filter(
        (ans) => ans.status === "skipped"
    );

    const totalTimeLeft = Object.values(questionResults).reduce(
        (prevVal, questionResults) => prevVal + (questionResults.timeLeft ?? 0),
        0
    );

    const totalTimeAllocated =
        questionLengths * 15 + (plus10Available ? 0 : 10);

    return questionLengths === Object.keys(questionResults).length ? (
        <div className="w-full p-5 flex-cols gap-2 items-center bg-white rounded drop-shadow-xl">
            <div>Hooray you completed the quiz 🎉</div>
            <div>Here is your overall summary</div>
            <div>{`You have got ${correct.length} of them correct!`}</div>
            <div>{`Which makes your total score of ${Math.round(
                (correct.length / questionLengths) * 100
            )} %`}</div>
            <div>{`While you had ${incorrect.length} incorrect ones`}</div>
            <div>{`You skipped ${skipped.length}`}</div>
            <div>{`You still had ${totalTimeLeft}s left on the clock`}</div>
            <div>{`Which on average you spent around ${
                (totalTimeAllocated - totalTimeLeft) / questionLengths
            }s per question`}</div>
        </div>
    ) : null;
};
