import { useState, useEffect, useRef, type FC } from "react";
import { useGetQuestionById } from "services/questions";
import { validateAnswer, validate5050 } from "services/validate";
import type { PossibleAnswers } from "types/questions";
import { usePageProvider, PageActions } from "contexts";
import { LoadingGuard } from "components/loading-guard";
import cx from "classnames";
import Image from "next/image";

const DEFAULT_TIMEOUT = 15;

interface QuestionProps {
    questionId: string;
    handleProceed(): void;
}

export const Question: FC<QuestionProps> = ({
    questionId,
    handleProceed,
}: QuestionProps) => {
    const [timerId, setTimerId] = useState<NodeJS.Timer | undefined>(undefined);
    const { data, status } = useGetQuestionById(questionId);
    const [timeLeft, setTimeleft] = useState<number>(DEFAULT_TIMEOUT);
    const startCountDown = useRef(() => {});
    const { dispatch, pageState } = usePageProvider();
    const { questionResults, fiftyFiftyAvailable, plus10Available } = pageState;
    const [resume, setResume] = useState(false);
    const [fiftyRes, setFiftyRes] = useState<string[]>([]);

    const handleValidate = async (answerId: string) => {
        clearInterval(timerId);
        await validateAnswer({
            dispatch,
            questionId,
            lockedInAnswer: answerId,
            timeLeft,
        });

        handleProceed();
    };

    const handleAdd10s = () => {
        dispatch({ type: PageActions.USE_PLUS_10 });
        setTimeleft((prevState) => prevState + 10);
    };

    const handle5050 = async () => {
        clearInterval(timerId);
        const response = await validate5050({ questionId, dispatch });
        setFiftyRes(response);
        setResume(true);
    };

    useEffect(() => {
        if (timeLeft <= 0) {
            clearInterval(timerId);
            dispatch({
                type: PageActions.SET_PROGRESS,
                payload: { questionId, status: "skipped" },
            });
            handleProceed();
        }
    }, [timeLeft, handleProceed, timerId, dispatch, questionId]);

    useEffect(() => {
        startCountDown.current = () =>
            setTimeleft((time) => {
                if (status === "success") return time - 1;
                else return time;
            });
    }, [status]);

    useEffect(() => {
        const id = setInterval(() => {
            startCountDown.current();
        }, 1000);

        setTimerId(id);
        return () => {
            clearInterval(id);
        };
    }, [setTimerId, resume]);

    return (
        <LoadingGuard status={status}>
            {data ? (
                <div
                    className={cx(
                        "px-4 pt-2 pb-[5rem] w-full rounded flex flex-col bg-white drop-shadow-xl",
                        {
                            "bg-gray-100": questionResults?.[questionId],
                            "border-green-500 bg-green-100":
                                questionResults?.[questionId]?.status ===
                                "correct",
                            "border-red-500 bg-red-100":
                                questionResults?.[questionId]?.status ===
                                "incorrect",
                        }
                    )}
                >
                    <div className="text-sm flex w-full justify-end">{`Time left : ${timeLeft}`}</div>

                    {data.question?.imageSrc && (
                        <div className="relative h-[20rem] w-full overflow-hidden bg-neutral-500 rounded-md ">
                            <Image
                                src={data.question.imageSrc}
                                layout="fill"
                                quality={100}
                                objectFit="cover"
                                alt={data.question.altText}
                            />
                        </div>
                    )}

                    <div className="text-center text-4xl py-4">
                        {data.question.question}
                    </div>

                    <div className="grid grid-cols-2 gap-2">
                        {data.question.possibleAnswers.map(
                            ({ id, value }: PossibleAnswers) => (
                                <button
                                    key={id}
                                    onClick={() => handleValidate(id)}
                                    disabled={Boolean(
                                        (fiftyRes.length &&
                                            !fiftyRes.includes(id)) ||
                                            questionResults?.[questionId]
                                    )}
                                    className={cx(
                                        "p-2 border-2 border-solid border-gray-500 rounded-md disabled:bg-gray-200 hover:bg-blue-700 hover:text-white hover:border-white",
                                        {
                                            "border-blue-500 !bg-blue-100":
                                                id ===
                                                questionResults?.[questionId]
                                                    ?.lockedInAnswer,
                                        }
                                    )}
                                >
                                    {value}
                                </button>
                            )
                        )}
                        {!questionResults?.[questionId] && (
                            <div className="flex flex-col col-span-2">
                                <div>Lifelines available</div>
                                <div className="flex p-4 w-full justify-evenly gap-2">
                                    <button
                                        data-testid="add-10-s"
                                        onClick={handleAdd10s}
                                        className="w-1/2 p-2 border-2 border-solid border-gray-500 rounded disabled:bg-gray-500 hover:bg-fuchsia-700 hover:text-white hover:border-white"
                                        disabled={!plus10Available}
                                    >
                                        Plus 10s
                                    </button>
                                    <button
                                        data-testid="perform-50-50"
                                        onClick={handle5050}
                                        className="w-1/2 p-2 border-2 border-solid border-gray-500 rounded disabled:bg-gray-500 hover:bg-fuchsia-700 hover:text-white hover:border-white"
                                        disabled={!fiftyFiftyAvailable}
                                    >
                                        50/50
                                    </button>
                                </div>
                            </div>
                        )}
                    </div>
                </div>
            ) : (
                <div>No data{timeLeft}</div>
            )}
        </LoadingGuard>
    );
};
