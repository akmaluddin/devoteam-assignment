import { type Dispatcher, PageActions } from "contexts";

export type ValidateAnswerInput = {
    dispatch: Dispatcher;
    lockedInAnswer?: string;
    timeLeft: number;
    questionId?: string;
};

export type ValidateResponse = {
    result: Omit<QuestionStatuses, "loading" | "skipped">;
};

export const validateAnswer = async ({
    dispatch,
    lockedInAnswer,
    timeLeft,
    questionId,
}: ValidateAnswerInput) => {
    dispatch({
        type: PageActions.SET_PROGRESS,
        payload: { questionId, status: "loading", lockedInAnswer, timeLeft },
    });
    const answer = await fetch("/api/validate", {
        method: "POST",
        body: JSON.stringify({
            questionId,
            lockedInAnswer,
        }),
    })
        .then((res) => {
            if (res.ok) {
                return res.json();
            }

            return res.json().then((err) => Promise.reject(err));
        })
        .then((data) => {
            dispatch({
                type: PageActions.SET_PROGRESS,
                payload: {
                    questionId,
                    status: data.result,
                    lockedInAnswer,
                    timeLeft,
                },
            });
        })
        .catch((err) => {
            dispatch({
                type: PageActions.SET_PROGRESS,
                payload: {
                    questionId,
                    status: "error",
                    lockedInAnswer,
                    timeLeft,
                },
            });
        });
};

export type Validate5050Input = {
    dispatch: Dispatcher;
    questionId: string;
};

export type Validate5050Response = {
    result: string[];
};

export const validate5050 = async ({
    questionId,
    dispatch,
}: Validate5050Input): Promise<string[]> => {
    dispatch({ type: PageActions.USE_50_50 });

    const response = await fetch("/api/validate/fifty", {
        method: "POST",
        body: JSON.stringify({
            questionId,
        }),
    })
        .then((res) => {
            if (res.ok) {
                return res.json();
            }

            return res.json().then((err) => Promise.reject(err));
        })
        .then((data: Validate5050Response) => {
            return data.result;
        })
        .catch((err) => {
            dispatch({ type: PageActions.RESTORE_50_50 });
            return [];
        });

    return response;
};
