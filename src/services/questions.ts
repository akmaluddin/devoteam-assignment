import { useQuery } from "../helpers/query";
import type { Question } from "../types/questions";

export type GetQuestionsResponse = {
    questions: string[];
};

export type GetQuestionByIdResponse = {
    questionId: string;
    question: Question;
};

export const useGetQuestions = () =>
    useQuery<GetQuestionsResponse>("/api/questions");

export const useGetQuestionById = (id?: string) =>
    useQuery<GetQuestionByIdResponse>(`/api/questions/${id}`, Boolean(id));
