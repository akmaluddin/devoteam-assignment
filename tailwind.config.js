module.exports = {
  content: ["./src/**/*.{ts,tsx,mdx}"],
  theme: {
    fontFamily: {
      display: [
        "Gantari",
        "Noto Sans",
        "Open Sans",
        "ui-sans-serif",
        "system-ui",
      ],
    },
  },
  plugins: [],
};
