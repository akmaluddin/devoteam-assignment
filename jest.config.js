const nextJest = require("next/jest");
const createJestConfig = nextJest({
  dir: "./",
});
const customJestConfig = {
  roots: ["<rootDir>/src"],
  moduleDirectories: ["node_modules", "<rootDir>/src"],
  testEnvironment: "jest-environment-jsdom",
  setupFiles: ["<rootDir>/setupTests.js"],
};
module.exports = createJestConfig(customJestConfig);
