/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,
  images: {
    loader: "akamai",
    path: "",
    domains: ["unsplash.com"],
  },
};

module.exports = nextConfig;
